export interface IUserInfoProps {
    placeholder: string;
    onClick: (value: string) => void
}
import * as React from 'react';
import {Input} from 'semantic-ui-react';
import {IUserInfoProps} from './UserInput.interface';

class UserInput extends React.Component<IUserInfoProps> {
    public state = {
        inputValue: ''
    };

    private inputElement: HTMLInputElement;

    private onKeyPressed = (e: KeyboardEvent) => {
        if (e.key === 'Enter') {
            this.props.onClick(this.state.inputValue);

            this.inputElement.value = '';
        }
    };

    private onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        event.preventDefault();

        if (this.inputElement === undefined) {
            this.inputElement = event.target;
        }

        const inputValue = this.inputElement.value;

        this.setState({inputValue})
    };

    public render() {
        return (
            <div className='user-input'>
                <Input placeholder={this.props.placeholder} onChange={this.onChange}
                       onKeyPress={this.onKeyPressed} fluid={true}/>
            </div>
        );
    }
}

export default UserInput;


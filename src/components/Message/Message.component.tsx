import * as React from 'react';
import * as Semantic from 'semantic-ui-react';
import {IMessageProps} from './Message.interface';

class Message extends React.Component<IMessageProps> {
    public render() {
        return (
            <Semantic.Message floating={true}>
                {this.props.text}
            </Semantic.Message>
        )
    }
}

export default Message;
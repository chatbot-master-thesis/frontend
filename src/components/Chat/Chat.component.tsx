import * as React from 'react';
import Message from '../Message/Message.component';
import axios from 'axios';
import './Chat.css';
import {Button} from 'semantic-ui-react';
import QuestionHelp from '../QuestionHelp/QuestionHelp.component';
import {IChatState} from './Chat.interface';
import QuestionAnswer from '../QuestionAnswer/QuestionAnswer.component';

class Chat extends React.Component<{}, IChatState> {

    public state: IChatState = {
        messages: [],
        answerState: false,
        questionId: 0
    };

    private static QUESTION = 'I want to ask a question';
    private static ANSWER = 'I want to give an answer';

    public componentDidMount(): void {
        this.loadQuestion();
    }

    private loadQuestion = async () => {
        const question = (await axios.get('http://localhost:8080/question/get')).data;

        const messages = this.state.messages.slice();

        messages.push(question);

        this.setState({messages, questionId: question.id});
    };

    private changeState = () => {
        this.setState({answerState: !this.state.answerState})
    };


    private userInputOnClick = (enteredText: string) => {
        const messages = this.state.messages.slice();
        messages.push({
            id: Math.random() * 1000,
            text: enteredText
        });

        this.setState({messages});
    };


    public render() {
        const messageComponents = this.state.messages.map(message => <Message key={message.id} text={message.text}/>);

        return (
            <div className='chat'>
                {messageComponents}
                <div className='chat-input'>
                    <QuestionHelp questionId={this.state.questionId} shouldRender={!this.state.answerState} onClick={this.userInputOnClick}/>
                    <QuestionAnswer questionId={this.state.questionId} shouldRender={this.state.answerState} onClick={this.userInputOnClick}/>
                    <Button content={!this.state.answerState ? Chat.ANSWER : Chat.QUESTION} onClick={this.changeState}/>
                </div>
            </div>
        );
    }

}

export default Chat;
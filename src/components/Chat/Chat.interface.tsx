import {IMessageProps} from '../Message/Message.interface';

export interface IChatState {
    messages: IMessageProps[],
    answerState: boolean,
    questionId: number
}
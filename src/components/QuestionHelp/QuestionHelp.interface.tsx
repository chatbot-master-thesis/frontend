export interface IQuestionHelpProps {
    shouldRender: boolean,
    onClick: (value: string) => void,
    questionId: number
}
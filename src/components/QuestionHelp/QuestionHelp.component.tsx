import * as React from 'react';
import axios from 'axios';
import UserInput from '../UserInput/UserInput.component';
import {IQuestionHelpProps} from './QuestionHelp.interface';

class QuestionHelp extends React.Component<IQuestionHelpProps> {
    private askQuestion = (text: string) => {
        this.props.onClick(text);

        const data = {
            'question': 'What attributes student have'
        };

        axios.post(`http://localhost:8080/question-help/${this.props.questionId}/ask`, data)
    };

    public render() {
        if (this.props.shouldRender) {
            return (<UserInput placeholder='Ask a question' onClick={this.askQuestion}/>);
        }

        return null;
    }
}

export default QuestionHelp;
import * as React from 'react';
import {IQuestionAnswerProps} from './QuestionAnswer.interface';
import axios from 'axios';
import UserInput from '../UserInput/UserInput.component';

class QuestionAnswer extends React.Component<IQuestionAnswerProps> {
    private saveAnswer = (text: string) => {
        this.props.onClick(text);

        const data = {
            'elements' : [
                {'@type': 'entity', 'name': 'test'}
            ]
        };

        axios.post(`http://localhost:8080/ermodel/${this.props.questionId}/answer`, data, {headers: {
                'Content-Type': 'application/json'
            }});
    };

    public render() {
        if (this.props.shouldRender) {
            return (<UserInput placeholder='Type an answer' onClick={this.saveAnswer}/>);
        }

        return null;
    }
}

export default QuestionAnswer;
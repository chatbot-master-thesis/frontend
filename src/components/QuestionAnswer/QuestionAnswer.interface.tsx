export interface IQuestionAnswerProps {
    shouldRender: boolean,
    onClick: (value: string) => void,
    questionId: number
}
import * as React from 'react';
import 'semantic-ui-css/semantic.min.css'
import Chat from './components/Chat/Chat.component';

class App extends React.Component {
    public render() {
        return (
            <Chat/>
        )
    }
}

export default App;
